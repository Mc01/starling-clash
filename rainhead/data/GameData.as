package rainhead.data 
{
	public class GameData 
	{	
		public static const SCREEN_WIDTH:int = 800;
		public static const SCREEN_HEIGHT:int = 600;
		
		public static var MOD_ALPHA:Boolean = false;
		public static var MOD_ROTATE:Boolean = false;
		public static var MOD_SCALE:Boolean = false;
	}
}