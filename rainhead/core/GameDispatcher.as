package rainhead.core 
{
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class GameDispatcher extends EventDispatcher 
	{
		public static const UPDATE_EVENT:String = "update_event";
		
		private static var _instance:GameDispatcher;
		
		public function GameDispatcher() 
		{
			super();	
			_instance = this;
		}
		
		public function dispatchNumber(dataNumber:int):void 
		{
			dispatchEventWith(UPDATE_EVENT, false, dataNumber);
		}
		
		static public function get instance():GameDispatcher 
		{
			return _instance;
		}
	}
}