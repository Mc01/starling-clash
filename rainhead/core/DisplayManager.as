package rainhead.core 
{
	import rainhead.actors.Mine;
	import rainhead.data.GameData;
	import rainhead.interfaces.IDisplay;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.textures.Texture;
	
	public class DisplayManager extends Sprite implements IDisplay
	{
		private var texture:Texture;
		private var mineVector:Vector.<Mine>;
		
		public function DisplayManager(texture:Texture) 
		{
			super();
			this.texture = texture;
			mineVector = new Vector.<Mine>();
			addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}	
		
		private function onEnterFrame(e:EnterFrameEvent):void 
		{
			for each (var element:Mine in mineVector) 
			{
				element.update();
			}
		}
		
		private function randomOne():int
		{
			return -1 + 2 * int(Math.random() * 2);
		}
		
		private function getAlpha():Number 
		{
			if (GameData.MOD_ALPHA) 
			{
				return Math.random() * 1;
			}
			else 
			{
				return 1;
			}
		}
		
		private function getRotation():Number 
		{
			if (GameData.MOD_ROTATE) 
			{
				return Math.random() * Math.PI * 2;
			}
			else 
			{
				return 0;
			}
		}
		
		private function getScale():Number 
		{
			if (GameData.MOD_SCALE) 
			{
				return Math.random() * 1;
			}
			else 
			{
				return 1;
			}
		}
		
		/* INTERFACE rainhead.interfaces.IDisplay */
		
		public function addElements(number:int):void 
		{
			var x:int;
			var y:int;
			var dx:int;
			var dy:int;
			var alpha:Number = 1;
			var da:Number = 0;
			var rotation:Number = 0;
			var dr:Number = 0;
			var scale:Number = 1;
			var ds:Number = 0;
			
			for (var i:int = 0; i < number; i++) 
			{
				x = Math.random() * GameData.SCREEN_WIDTH;
				y = Math.random() * GameData.SCREEN_HEIGHT;
				dx = randomOne();
				dy = randomOne();
				
				alpha = getAlpha();
				da = randomOne() * 0.01;
				
				rotation = getRotation();
				dr = randomOne() * 0.1;
				
				scale = getScale();
				ds = -0.01;
				
				mineVector.push(new Mine(texture, x, y, dx, dy, alpha, da, rotation, dr, scale, ds));
				addChild(mineVector[mineVector.length - 1]);
			}
			
			GameDispatcher.instance.dispatchNumber(mineVector.length);
		}
		
		public function removeElements(number:int):void 
		{
			number = mineVector.length - 1 - number;
			for (var i:int = mineVector.length - 1; i > number && i > -1 ; i--) 
			{
				removeChild(mineVector[i]);
				mineVector.splice(i, 1);
			}
			
			GameDispatcher.instance.dispatchNumber(mineVector.length);
		}
		
		public function updateAlpha():void 
		{
			for each (var element:Mine in mineVector) 
			{
				element.alpha = getAlpha();
			}
		}
		
		public function updateRotation():void 
		{
			for each (var element:Mine in mineVector) 
			{
				element.rotation = getRotation();
			}
		}
		
		public function updateScale():void 
		{
			for each (var element:Mine in mineVector) 
			{
				element.scaleX = getScale();
				element.scaleY = element.scaleX;
			}
		}
	}
}