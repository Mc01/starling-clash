package rainhead.core 
{
	import flash.system.System;
	import flash.ui.Keyboard;
	import rainhead.actors.Background;
	import rainhead.actors.Mine;
	import rainhead.actors.Status;
	import rainhead.data.GameData;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.textures.Texture;
	
	public class GameManager extends Sprite 
	{
		[Embed(source = "../../../assets/cosmos.png")]
		private static const CosmosPNG:Class;
		
		[Embed(source = "../../../assets/mine_o.png")]
		private static const MinePNG:Class;
		
		private var displayManager:DisplayManager;
		private var dispatcher:GameDispatcher;
		
		public function GameManager() 
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			init();
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}	
		
		private function init():void 
		{
			dispatcher = new GameDispatcher();
			
			var backgroundTexture:Texture = Texture.fromBitmap(new CosmosPNG());
			var background:Background = new Background(GameData.SCREEN_WIDTH, GameData.SCREEN_HEIGHT, backgroundTexture);
			addChild(background);
			
			var mineTexture:Texture = Texture.fromBitmap(new MinePNG());
			displayManager = new DisplayManager(mineTexture);
			addChild(displayManager);
			
			var status:Status = new Status();
			addChild(status);
		}
		
		private function onKeyDown(e:KeyboardEvent):void 
		{
			switch (e.keyCode) 
			{
				case Keyboard.LEFT:
					displayManager.removeElements(1);
				break;
				case Keyboard.UP:
					displayManager.addElements(10);
				break;
				case Keyboard.RIGHT:
					displayManager.addElements(1);
				break;
				case Keyboard.DOWN:
					displayManager.removeElements(10);
				break;
				case Keyboard.A:
					GameData.MOD_ALPHA = toggle(GameData.MOD_ALPHA);
					displayManager.updateAlpha();
				break;
				case Keyboard.R:
					GameData.MOD_ROTATE = toggle(GameData.MOD_ROTATE);
					displayManager.updateRotation();
				break;
				case Keyboard.S:
					GameData.MOD_SCALE = toggle(GameData.MOD_SCALE);
					displayManager.updateScale();
				break;
			}
		}
		
		private function toggle(bool:Boolean):Boolean 
		{
			if (bool) 
			{
				return false;
			}
			else 
			{
				return true;
			}
		}
	}
}