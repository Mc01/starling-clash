package rainhead
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import rainhead.core.GameManager;
	import starling.core.Starling;

	[Frame(factoryClass="rainhead.Preloader")]
	public class Main extends Sprite 
	{
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var starling:Starling = new Starling(GameManager, stage);
			starling.start();
		}
	}
}