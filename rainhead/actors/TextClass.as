package rainhead.actors 
{
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class TextClass extends TextField 
	{
		public function TextClass(width:int, height:int, text:String, x:int, y:int) 
		{
			super(width, height, text, BitmapFont.MINI, 8, 0xffffff, false);	
			this.x = x;
			this.y = y;
			this.hAlign = HAlign.LEFT;
			this.vAlign = VAlign.TOP;
		}	
	}
}