package rainhead.actors 
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class Background extends Sprite 
	{
		public function Background(width:int, height:int, texture:Texture) 
		{
			super();
			var background:Image;
			
			for (var i:int = 0; i < width; i += texture.width) 
			{
				for (var j:int = 0; j < height; j += texture.height) 
				{
					background = new Image(texture);
					background.x = i;
					background.y = j;
					addChild(background);
				}
			}
		}
	}
}