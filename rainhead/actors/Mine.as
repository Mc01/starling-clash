package rainhead.actors 
{
	import rainhead.data.GameData;
	import rainhead.interfaces.IUpdate;
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class Mine extends Image implements IUpdate
	{
		private var dx:int;
		private var dy:int;
		private var da:Number;
		private var dr:Number;
		private var ds:Number;
		
		public function Mine(texture:Texture, x:int, y:int, dx:int, dy:int, alpha:Number = 1, da:Number = 0, rotation:Number = 0, dr:Number = 0, scale:Number = 1, ds:Number = 0) 
		{
			super(texture);
			this.pivotX = this.width / 2;
			this.pivotY = this.height / 2;
			this.x = x;
			this.y = y;
			this.dx = dx;
			this.dy = dy;
			this.alpha = alpha;
			this.da = da;
			this.rotation = rotation;
			this.dr = dr;
			this.scaleX = scale;
			this.scaleY = scale;
			this.ds = ds;
		}
		
		/* INTERFACE rainhead.core.IUpdate */
		
		public function update():void 
		{
			modX();
			modY();
			checkMods();
		}
		
		private function modX():void 
		{
			x -= dx;
			dx = checkBounds(x, 0, GameData.SCREEN_WIDTH, dx);
		}
		
		private function modY():void 
		{
			y -= dy;
			dy = checkBounds(y, 0, GameData.SCREEN_HEIGHT, dy);
		}
		
		private function checkMods():void 
		{
			if (GameData.MOD_ALPHA) 
			{
				alpha -= da;
				da = checkBounds(alpha, 0, 1, da);
			}
			
			if (GameData.MOD_ROTATE) 
			{
				rotation += dr;
				if (rotation > Math.PI * 2) 
				{
					dr = rotation -= Math.PI * 2;
				}
			}
			
			if (GameData.MOD_SCALE) 
			{
				scaleX -= ds;
				scaleY -= ds;
				ds = checkBounds(scaleX, 0.1, 1, ds);
			}
		}
		
		private function checkBounds(value:Number, left:Number, right:Number, delta:Number):Number 
		{
			if (value - delta <= left)
			{
				return -delta;
			}
			if (value - delta >= right) 
			{
				return -delta;
			}
			else 
			{
				return delta;
			}
		}
	}
}