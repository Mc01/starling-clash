package rainhead.actors 
{
	import rainhead.core.GameDispatcher;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	
	public class Status extends Sprite 
	{
		private var objText:TextClass;
		private var timeText:TextClass;
		private var frames:int = 0;
		private var time:Number = 0;
		
		public function Status() 
		{
			super();
			timeText = new TextClass(70, 50, "TP60: 0", 2, 1);
			addChild(timeText);
			objText = new TextClass(50, 50, "OBJ: 0", 2, 9);
			addChild(objText);
			addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			GameDispatcher.instance.addEventListener(GameDispatcher.UPDATE_EVENT, onUpdate);
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void 
		{
			time += e.passedTime;
			if (++frames == 60) 
			{
				timeText.text = "TP60: " + time.toPrecision(3);
				frames = time = 0;
			}
		}
		
		private function onUpdate(e:Event):void 
		{
			var number:int = int(e.data);
			objText.text = "OBJ: " + number;
		}
	}
}