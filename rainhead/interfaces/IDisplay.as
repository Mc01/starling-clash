package rainhead.interfaces 
{
	public interface IDisplay 
	{
		function addElements(number:int):void;
		function removeElements(number:int):void;
		
		function updateAlpha():void;
		function updateRotation():void;
		function updateScale():void;
	}	
}